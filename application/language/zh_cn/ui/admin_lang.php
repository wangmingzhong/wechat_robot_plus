<?php

$lang['ui_admin'] = '管理面板';

$lang['ui_admin_hi'] = '您好';

$lang['confirm_to_delete'] = '确认删除吗？';

//黑名单管理
$lang['blacklists_manage'] = '屏蔽列表管理';
$lang['blacklists_list'] = '屏蔽列表';
$lang['add_blacklist'] = '新增屏蔽';
$lang['banned_time'] = '屏蔽时间';
$lang['user_opn_id_note'] = '用户的公众平台open id，例如opexxx_xxxxxxxx_xx';

$lang['blacklists_add_success'] = '屏蔽新增完成';
$lang['blacklists_edit_success'] = '屏蔽编辑完成';
$lang['blacklists_del_success'] = '屏蔽删除完成';

//用户管理
$lang['user_manage'] = '用户管理';
$lang['users_list'] = '用户列表';
$lang['add_user'] = '新增用户';
$lang['username_rule_note'] = '以字母开头，可用字母及数字，长度6至20';
$lang['password_rule_note'] = '可用下划线、字母及数字，长度6至20';
$lang['edit_password_rule_note'] = '可用下划线、字母及数字，长度6至20，不修改请留空';

$lang['user_add_success'] = '用户新增完成';
$lang['user_del_success'] = '用户删除完成';
$lang['user_edit_success'] = '用户编辑完成';

//消息类型管理
$lang['msgtype_manage'] = '消息类型管理';
$lang['msgtype_list'] = '消息类型列表';
$lang['add_msgtype'] = '新增消息类型';

$lang['msg_type_desc'] = '类型描述';
$lang['msg_type_for_reply'] = '回复可用';

$lang['msg_type_note'] = '消息类型名称，用于区别用户消息的类型决定处理方式';
$lang['msg_type_desc_note'] = '类型描述';
$lang['msg_type_for_reply_note'] = '该类型是否可用于回复用户消息';

$lang['msgtype_add_success'] = '消息类型新增完成';
$lang['msgtype_del_success'] = '消息类型删除完成';
$lang['msgtype_edit_success'] = '消息类型编辑完成';

//关键字管理
$lang['keywords_manage'] = '关键字管理';
$lang['keywords_list'] = '关键字列表';
$lang['add_keyword'] = '新增关键字';

$lang['for_msgtype'] = '对应类型';

$lang['for_msgtype_note'] = '关键字所属消息类型';

$lang['keyword_add_success'] = '关键字新增完成';
$lang['keyword_del_success'] = '关键字删除完成';
$lang['keyword_edit_success'] = '关键字编辑完成';

//回复管理
$lang['reply_manage'] = '回复管理';
$lang['reply_list'] = '回复列表';
$lang['add_reply'] = '新增回复';
$lang['reply_msgtype'] = '回复类型';
$lang['reply_keyword_note'] = '输入关键字自动补全并转换成Id';
$lang['reply_content'] = '文字回复';
$lang['pic_url'] = '图片链接';
$lang['link_url'] = '页面链接';
$lang['music_url'] = '音频链接';
$lang['hqmusic_url'] = '高品质音频链接';

$lang['reply_add_success'] = '回复新增完成';
$lang['reply_del_success'] = '回复删除完成';
$lang['reply_edit_success'] = '回复编辑完成';

//命令管理
$lang['parent_command'] = '父命令';
$lang['data_regex'] = '数据格式';

$lang['commands_manage'] = '命令管理';
$lang['commands_list'] = '命令列表';
$lang['add_command'] = '新增命令';

$lang['check_expire'] = '检查超时';
$lang['check_expire_note'] = '检查接收到用户两次命令之间的间隔是否大于超时设置时间';

$lang['use_plugin'] = '使用插件';
$lang['use_plugin_note'] = '是否使用外部插件处理用户数据';
$lang['plugin_name'] = '插件名称';
$lang['plugin_name_note'] = '数据处理插件名';
$lang['plugin_function'] = '入口函数';
$lang['plugin_function_note'] = '插件的入口函数，调用时会将用户数据对象传递到该函数上';

$lang['command_add_success'] = '命令新增完成';
$lang['command_del_success'] = '命令删除完成';
$lang['command_edit_success'] = '命令编辑完成';