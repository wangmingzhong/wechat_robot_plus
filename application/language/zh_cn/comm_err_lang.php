<?php

$lang['error'] = '错误';

$lang['err_invalid_data'] = '无效的数据';
$lang['err_no_data'] = '暂无数据';

$lang['err_file_upload_failed'] = '文件上传失败';

$lang['err_nologin'] = '用户未登录';
$lang['err_user_not_exist'] = '用户不存在';
$lang['err_wrong_password'] = '用户名与密码不匹配';

$lang['err_username_not_match_rule'] = '用户名组成不符合规则';
$lang['err_password_not_match_rule'] = '密码组成不符合规则';
$lang['err_password_repassword_not_match'] = '两次密码输入不一致';
$lang['err_username_already_used'] = '用户名已被占用';

$lang['err_user_add_fail'] = '用户添加失败';
$lang['err_user_del_fail'] = '删除用户失败<br />'
        . '可能原因如下：<br />'
        . '◆指定用户是本系统唯一管理员权限角色<br />'
        . '◆数据库错误';
$lang['err_user_edit_fail'] = '用户编辑失败<br />'
        . '可能原因如下：<br />'
        . '◆指定用户是本系统唯一管理员权限角色<br />'
        . '◆数据库错误';

$lang['err_user_opn_id_not_match_rule'] = '用户opon id应为字母数字及下划线组成的28位字符串';
$lang['err_blacklists_not_exist'] = '指定的用户open id没有被屏蔽';
$lang['err_blacklists_add_failed'] = '新增屏蔽失败<br />'
        . '可能原因如下：<br />'
        . '◆指定的open id已存在<br />'
        . '◆数据库错误';
$lang['err_blacklists_del_failed'] = '删除屏蔽失败<br />';

$lang['err_msgtype_not_match_rule'] = '消息类型名组成不符合规则';
$lang['err_msgtype_already_exist'] = '消息类型已存在';
$lang['err_msgtype_not_exist'] = '指定的消息类型不存在';
$lang['err_msgtype_add_failed'] = '新增消息类型失败<br />';
$lang['err_msgtype_del_failed'] = '删除消息类型失败<br />';
$lang['err_msgtype_edit_failed'] = '编辑消息类型失败<br />';

$lang['err_keyword_not_exist'] = '指定的关键字不存在';
$lang['err_keyword_not_match_rule'] = '关键字组成不符合规则';
$lang['err_keyword_del_failed'] = '删除关键字失败<br />';
$lang['err_keyword_add_failed'] = '新增关键字失败<br />';
$lang['err_keyword_edit_failed'] = '编辑关键字失败<br />';

$lang['err_reply_not_exist'] = '指定的回复不存在';
$lang['err_reply_del_failed'] = '删除回复失败<br />';
$lang['err_reply_add_failed'] = '新增回复失败<br />';
$lang['err_reply_edit_failed'] = '编辑回复失败<br />';

$lang['err_command_not_exist'] = '指定的命令不存在';
$lang['err_command_del_failed'] = '删除命令失败<br />';
$lang['err_command_add_failed'] = '新增命令失败<br />';
$lang['err_command_edit_failed'] = '编辑命令失败<br />';