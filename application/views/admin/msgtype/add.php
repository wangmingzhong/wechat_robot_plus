<div id="win_r">
    <form action="<?php echo site_url('admin/msgtype_doadd'); ?>" method="post">
        <ul>
            <li>
                <span class="title"><?php echo lang('msg_type'); ?></span>
                <input type="text" name="type_name" class="input" maxlength="10" />
                <span class="m_left_10 notice"><?php echo lang('msg_type_note'); ?></span>
            </li>
            <li>
                <span class="title"><?php echo lang('msg_type_desc'); ?></span>
                <textarea name="description" class="input_area" maxlength="100"></textarea>
            </li>
            <li>
                <span class="title"><?php echo lang('msg_type_for_reply'); ?></span>
                <select class="input" name="for_reply">
                    <option value="0"><?php echo lang('no'); ?></option>
                    <option value="1"><?php echo lang('yes'); ?></option>
                </select>
                <span class="m_left_10 notice"><?php echo lang('msg_type_for_reply_note'); ?></span>
            </li>
            <li class="text_c">
                <input type="submit" value="<?php echo lang('add'); ?>" onclick="submit();" />
            </li> 
        </ul>
    </form>
</div>