<div id="win_r">
    <table cellpadding="0" cellspacing="0" class="text_c">
        <tr class="table_title">
            <td style="width: 200px"><?php echo lang('username'); ?></td>
            <td style="width: 100px"><?php echo lang('authority'); ?></td>
            <td style="width: 200px"><?php echo lang('operation'); ?></td>
        </tr>
        <?php
        if (is_array($users)) {
            foreach ($users as $v) {
                $tmp = '<tr>'
                        . '<td>' . $v->username . '</td>'
                        . '<td>' . $v->group . '</td>'
                        . '<td>'
                        . anchor(site_url('admin/user_edit/' . $v->id), lang('edit'))
                        . ' | '
                        . anchor(site_url('admin/user_dodel/' . $v->id), lang('delete'), array(
                            'onclick' => 'if(false===confirm(\'' . lang('confirm_to_delete') . '？\')){return false;}'
                        ))
                        . '</td>';
                echo $tmp;
            }
        } else {
            echo lang('err_no_data');
        }
        ?>
    </table>
    <?php echo $pages; ?>
</div>