<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title> <?php echo lang('ui_login') . ' - ' . lang('site_title'); ?> </title>
        <meta name="Author" content="terminus">
        <meta name="Keywords" content="<?php echo lang('site_keyword'); ?>">
        <meta name="Description" content="<?php echo lang('site_description'); ?>">
        <link type="text/css" href="<?php echo public_res('css/style.css'); ?>" rel="stylesheet" />
        <script type="text/javascript" src="<?php echo public_res('js/jquery-1.9.1.min.js'); ?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('input[name="username"]').focus();
            });
        </script>
    </head>

    <body>
        <div id="container">
            <div id="login_form">
                <form action="<?php echo site_url('login/dologin'); ?>" method="post">
                    <ul>
                        <li>
                            <span class="title"><?php echo lang('ui_login_username'); ?></span>
                            <input type="text" name="username" class="input" maxlength="20" />
                        </li> 
                        <li>
                            <span class="title"><?php echo lang('ui_login_password'); ?></span>
                            <input type="password" name="password" class="input" maxlength="20" />
                        </li> 
                        <li class="text_r">
                            <input type="submit" value="<?php echo lang('login'); ?>" onclick="submit();" />
                        </li> 
                    </ul>
                </form>
            </div>
        </div>
    </body>

</html>
